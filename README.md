# Audio File Playback App
- Simple Electron App for audio file playback
- Main feature is audio file tagging for easy music finding
- Tags can easily be added/removed to make searching for a specific type of track easier
- Slightly easier to manage local audio files without uploading to something like Spotify

## Sample Image
![Audio File App](https://lh3.googleusercontent.com/hZ4LcqFsQXPDlm5r1tuipUaUphUmY5f-K5LQTPcn3GwWwU3fupMvgIAsOYApszxG188jyUNC6f0wOE4P8XXvfPRqqZ0o1g6BL2-4hIEjrwp6BrRvFm4-xPOio19W40tsGh_0VYOqmZkMM411jsKCvY-6v7CFMGxQfn2i9uqW5DDd33Jp8ItGUTLHsZ4SgoJXsCl8WS_3EnPzzvw1rpUvAIMNGiy4MfbZMvcsj7axjjnD85saSiCQzzzEfvRZAhfitLHul2-ezZGvTtnHTTpOEwx3Xi_Uf-Y2yafDl2Bpb51yCfZ7JG8nJ2DhYbSWsoQjckx1bI_vHYktXlG_5gVvCCCFU0oLC2VnXjUhzPr129NOXES83zrYoVsYIMAxMZAo11HoQmUXHUAZ6l2Eq8cCIU6Dt3eCKt_eqxBE76wjFbbq7AjXBpR-Ep00fCfmuHUKme3H3NL6O3_aXZysGwxonbyKW4SUEmK2awkjmgm3S5DOmS0Wji9Myd_hBs30w6J9wBEqFSoDoSZUpT02zpb0nveERR8NFbUOOCeNMqPutGa3qv__35z4M0UO4SwCl3SwpfNQU1eLanXWlZkqHMjK3YEkEb-0D4u4jrdfJo4=w1433-h1048-no)
