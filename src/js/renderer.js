const electron = require('electron');
const ipc = electron.ipcRenderer;

const AudioManager = require('./audio/audio');
const InfoManager = require('./info/info');

class APP {
    constructor () {
        this.audio_manager = new AudioManager();
        this.info_manager = new InfoManager();

        this.setIPCEvents();
        this.setEvents();

        ipc.send('app_loaded');
    }

    setIPCEvents () {
        // Audio Data is loaded first, then the files
        ipc.on('data_loaded', (e, data_json) => {
            this.audio_manager.data.set(JSON.parse(data_json));
        });

        // Files are loaded second
        ipc.on('files_loaded', (e, files_json) => {
            this.audio_manager.buildAudioList(JSON.parse(files_json));
        });
    }

    setEvents () {
        // Array destructuring because normally its an HTMLCollection
        [...document.getElementsByClassName('tab')].forEach((tab) => {
            tab.addEventListener('click', (e) => {
                const el_tab = e.currentTarget;
                [...document.getElementsByClassName('tab')].forEach((tab) => {
                    tab.classList.remove('active');
                });
                el_tab.classList.add('active');

                const container_name = el_tab.getAttribute('data-container');
                [...document.getElementsByClassName('container')].forEach((container) => {
                    container.classList.remove('active');
                    if (container.getAttribute('id') === container_name) {
                        container.classList.add('active');
                    }
                });
            });
        });
    }
};

window.onload = () => {
    window.APP = new APP();
}
