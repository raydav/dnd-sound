function createElement (type, classes, opts) {
    opts = opts || {};
    let node = document.createElement(type);
    let classes_split = classes.split(' ');
    for (let i = 0; i < classes_split.length; ++i) {
        node.classList.add(classes_split[i]);
    }
    if (opts.attributes) {
        for (let attr in opts.attributes) {
            if (opts.attributes[attr]) {
                node.setAttribute(attr, opts.attributes[attr]);
            }
        }
    }
    if (opts.dataset) {
        for (let data in opts.dataset) {
            if (opts.dataset[data]) {
                node.dataset[data] = opts.dataset[data];
            }
        }
    }
    if (opts.events) {
        for (let event in opts.events) {
            node.addEventListener(event, opts.events[event]);
        }
    }
    if (opts.html) {
        node.innerHTML = opts.html;
    }
    if (opts.addTo) {
        if (!opts.addTo.length) opts.addTo = [opts.addTo];
        opts.addTo.forEach((container) => {
            container.appendChild(node);
        });
    }
    if (opts.css) {
        for (let style in opts.css) {
            node.style[style] = opts.css[style];
        }
    }
    return node;
}

function cacheElements (obj, cache_list) {
    cache_list.forEach((id) => {
        obj['el_' + id] = document.getElementById(id);
    });
}

module.exports = {
    createElement,
    cacheElements
};