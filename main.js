const electron = require('electron');
// Module to control application life.
const app = electron.app
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow
const IPC = electron.ipcMain;

const FileHelpers = require('./lib/file_helpers');

const fs = require('fs');
const path = require('path');
const url = require('url');

const base_dir = 'audio_tracks';
let media_dir = '';

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow = null;

function createWindow () {
    media_dir = FileHelpers.generateMediaDir({
        app: app,
        base: __dirname,
        base_dir: base_dir
    });

    // Create the browser window.
    mainWindow = new BrowserWindow({
        width: 1400,
        height: 800,
        icon: __dirname + '/favicon.ico'
    });
    mainWindow.setMenu(null);
    mainWindow.setPosition(100, 10);

    // and load the index.html of the app.
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }));

    // Open the DevTools.
    // mainWindow.webContents.openDevTools();

    // Emitted when the window is closed.
    mainWindow.on('closed', () => {
        mainWindow = null;
    });
}

IPC.on('app_loaded', (e) => {
    loadAudioData();
    const tracks = FileHelpers.generateList(media_dir, __dirname, base_dir);
    mainWindow.webContents.send('files_loaded', JSON.stringify(tracks));
});

IPC.on('save_audio_data', (e, audio_data) => {
    try {
        fs.writeFileSync(`${media_dir}/audio_data.json`, JSON.stringify(audio_data, null, 4), 'utf-8');
    } catch (e) {
        console.log('Failed to save the file !');
    }
});

function loadAudioData () {
    try {
        let audio_data = fs.readFileSync(`${media_dir}/audio_data.json`, {
            encoding: 'utf-8'
        });
        mainWindow.webContents.send('data_loaded', JSON.stringify(audio_data));
    } catch (e) {
        console.log('No audio_file.json file found');
    }
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
    app.quit()
})
