const packager = require('electron-packager');
packager({
    dir: ".",
    overwrite: true,
    platform: 'darwin',
    arch: 'x64',
    prune: true,
    out: 'build',
    ignore: [
         /\/audio$/,
         /\/audio_less$/,
         /\/package-lock\.json$/,
         /\/scripts$/
    ]
});
