const fs = require('fs');
const path = require('path');
const url = require('url');

const FileHelpers = {
    LOCAL: false,

    isLocal: function () {
        return this.LOCAL;
    },

    generateMediaDir: function (opts = {}) {
        let media_dir = '';
        let app_path = opts.app.getPath('exe');
        if (app_path.indexOf('node_modules') !== -1) {
            this.LOCAL = true;
            media_dir = path.join(opts.base, opts.base_dir);
        } else {
            this.LOCAL = false;
            let app_split = (process.platform === 'darwin') ? ".app" : ".exe";
            let dir_split = (process.platform === 'darwin') ? "/" : "\\";
            let app_path_split = app_path.split(app_split)[0].split(dir_split);
            app_path_split.pop();
            app_path_split.push(opts.base_dir);
            media_dir = app_path_split.join(dir_split);
        }

        if (!fs.existsSync(media_dir)) {
            fs.mkdirSync(media_dir);
        }

        return media_dir;
    },

    generateList: function (dir, base, base_dir) {
        let list = {};
        this.readDir(list, dir, base, base_dir);
        return list;
    },

    readDir: function (structure, dir, base, base_dir) {
        fs.readdirSync(dir).forEach((file) => {
            if (file.indexOf('(exclude)') !== -1) return;
            // The fuil path to the current thing we are checking
            // This can be either a folder or an image (or something we want to ignore)
            let full_path = dir + '/' + file;
            if (fs.lstatSync(full_path).isDirectory()) {
                // We found a directory, add it to the structure and dive in
                structure[file] = {
                    type: 'directory',
                    name: file
                };
                this.readDir(structure[file], full_path, base, base_dir);
            } else if (this.isAudio(file)) {
                structure.files = structure.files || {};
                // We found something that is not a directory
                let fixed_path = file;
                let file_name = file.split('.')[0];

                let relative_directory = dir.split(base_dir)[1];
                let thing = relative_directory.replace('/', '');
                relative_directory = base_dir + relative_directory;

                let key = thing + '/' + file_name;
                key = JSON.parse(JSON.stringify(key));
                const re = new RegExp(String.fromCharCode(160), "g");
                key = key.replace(re, " ");
                key = key.toLowerCase();
                key = key.replace(/ /g,'_');
                key = key.replace(/\//g,'_');
                key = key.replace(/__/g,'_');
                key = key.replace(/-/g,'');
                key = key.replace(/\(/g,'');
                key = key.replace(/\)/g,'');
                key = key.replace(/\./g,'');
                key = key.replace(/'/g,'');
                key = key.replace(/,/g,'');

                if (this.LOCAL) {
                    fixed_path = path.join(base, relative_directory + '/' + fixed_path);
                } else {
                    relative_directory = relative_directory.replace(base_dir, '');
                    fixed_path = map_dir + '/' + relative_directory + '/' + fixed_path;
                }

                let formatted_name = file_name;
                formatted_name = formatted_name.replace(/_/g, ' ');
                formatted_name = formatted_name.replace(/^[-\d\s]*/,"");

                structure.files[file_name] = {
                    type: 'audio',
                    key: key,
                    name: formatted_name,
                    path: fixed_path,
                };
            }
        });
    },

    isAudio: function (file) {
        if (file.match(/mp3|mp3|wav/)) return true;
        return false;
    }
};
module.exports = FileHelpers;
